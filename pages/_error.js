import React from 'react';
import Link from 'next/link';

const errorIndexPage = () => (
  <div>
    <h1>Something went wrong</h1>
    <p>Try <Link href="/"><a>go home</a></Link></p>
  </div>
)

export default errorIndexPage;
