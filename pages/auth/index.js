import React from 'react';

import User from '../../components/User';

const authIndexPage = () => (
  <div>
    <h1>The Auth Page</h1>
    <User name="Roman" age={42}></User>
  </div>
)

export default authIndexPage;
